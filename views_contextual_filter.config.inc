<?php

/**
 * @file
 * The configuration page functions
 *
 */

/**
 *  Generate "config form"
 * 
 *  @return
 *    form array
 */
function vcf_config_form_generate() {
  // query database
  $t_vocabs= vcf_query_tax_vocabs();
  $t_terms = vcf_query_tax_terms();
  
  // prepare variables  
  $conf_ignore = variable_get('views_contextual_filter_conf_ignore', 'all');
  $conf_argcount = variable_get('views_contextual_filter_conf_argcount', '1');
  $conf_redirect = variable_get('views_contextual_filter_conf_url', 'stay_on_page');
  
  // restore defaults
  $conf_args = variable_get('views_contextual_filter_conf_args', array('1'));
  $conf_titles = variable_get('views_contextual_filter_conf_titles', array('title'));
  
  // prepare form options
  $options['custom'] = t('-custom-');
  $i = 0;
  foreach ($t_vocabs as $vocab) {
    $vocab= get_object_vars($vocab);
    $options[$vocab['vid']] = $vocab['name'];
    if (!array_key_exists($i, $conf_titles)) {
      $conf_titles[$i] = 'argument' . $i;
    }
    if (!array_key_exists($i, $conf_args)) {
      $conf_args[$i] = $conf_ignore;
    }
    $i++;
  }

  // _____generate config form_______
  $form['vcf_argcount'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of arguments'),
    '#description' => t('This number will set number of accepted arguments and modify the form below'),
    '#default_value' => $conf_argcount,
    '#size' => 40,
  );
  $form['vcf_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Redirect url'),
    '#description' => t('Don\'t use leading or trailing "/". Default value: "stay_on_page"'),
    '#default_value' => $conf_redirect,
    '#size' => 40,
  );
  $form['vcf_ignore'] = array(
    '#type' => 'textfield',
    '#title' => t('Ignore value (if none is checked)'),
    '#description' => t('Default value: "all"'),
    '#default_value' => $conf_ignore,
    '#size' => 40,
  );
  // arguments
  $form['vcf_arg'] = array(
      '#type' => 'fieldset',
      '#title' => 'arguments',
      '#tree' => TRUE,
    );
  $form['vcf_arg']['markup'] = array(
  '#markup' => t('Select filter form options, in order in which you want them in your url. 
                  Choose taxonomy vocabularies or enter custom options.'),
  );
  for ($i=0; $i < $conf_argcount; $i++) {
    $form['vcf_arg'][$i] = array(
      '#type' => 'fieldset',
      '#title' => 'argument' . ' ' . ($i + 1),
    );
    $form['vcf_arg'][$i]['title'] = array(
      '#type' => 'textfield',
      '#title' => t('element title'),
      '#size' => 35,
      '#default_value' => $conf_titles[$i],
    );
    $form['vcf_arg'][$i]['value'] = array(
      '#type' => 'select',
      '#title' => t('values'),
      '#default_value' => $conf_args[$i],
      '#options' => $options
    );
    // custom arguments
    $form['vcf_arg'][$i]['custom'] = array(
      '#type' => 'fieldset',
      '#states' => array(
        'visible' => array(
           ':input[name="vcf_arg[' . $i . '][value]"]' => array('value' => 'custom')
        )
      )
    );
    $form['vcf_arg'][$i]['custom']['markup'] = array(
      '#markup' => t('Enter custom options in key-value pairs'),
    );
    $form['vcf_arg'][$i]['custom']['values'] = array(
      '#type' => 'textarea',
      '#default_value' => $conf_args[$i],
      '#description' => t('Use format: "key1=value1,key2=value2"'),
    );
  }
  return $form;
}


/**
 * Submit handler for "config block"
 */
function vcf_config_form_process($form_state) {
  // read config form
  $vcf_url = $form_state['vcf_url'];
  $vcf_ignore = $form_state['vcf_ignore'];
  $vcf_argcount = $form_state['vcf_argcount'];
  $vcf_titles = array();
  $vcf_args = array();
  
  foreach ($form_state['vcf_arg'] as $arg) {
    $vcf_titles[] = $arg['title'];
    if ($arg['value'] != 'custom') {
      $vcf_args[] = $arg['value'];
    } 
    else {
      $vcf_args[] = $arg['custom']['values'];
    }
  }
  
  // if user changed number of arguments, set some default values
  $count = count($vcf_args);
  if ($vcf_argcount > $count) {
    $diff = $vcf_argcount - $count;
    for ($i=0; $i < $diff; $i++) {
      $vcf_args[] = 'key1=value1,key2=value2';
    }
  }
  $count = count($vcf_titles);
  if ($vcf_argcount > $count) {
    $diff = $vcf_argcount - $count;
    for ($i=0; $i < $diff; $i++) {
      $vcf_titles[] = 'title';
    }
  }
  
  // save config
  $vcf_config['argcount'] = $vcf_argcount;
  $vcf_config['titles'] = $vcf_titles;
  $vcf_config['args'] = $vcf_args;
  $vcf_config['url'] = $vcf_url;
  $vcf_config['ignore'] = $vcf_ignore;
  
  variable_set('views_contextual_filter_config', $vcf_config );
}
