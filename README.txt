Views contextual filter
_____________________________
Info

    Module creates a block for filtering views that use contextual filters.
    Block consits of a form that will send url arguments to your view.
    Form can be customized on blocks->customize page.
_____________________________
Customization options

    Number of arguments - must be initially set and submitted for the right number of argument configuration fields to be displayed
    Form options - will be populated with terms from selected taxonomy vocabularies or with custom key=value pairs.
    Redirect url - form will redirect on submit with appended url parameters to provided url or stay on page (default is "stay_on_page")
    Ignore value - custom string used by views contextual filters as an ignore value (default is "all")

_____________________________
Notes

    Url arguments are Unique Term IDs (in case of Taxonomy) or custom keys
    Order of arguments is determined by the order of arguments on config page.
    Currently - supports only checkboxes as user input.
    Currently - remembers last user search.
