<?php

/**
 * @file
 * Functions used by the filter block
 */

/**
 *  Generate "Filter form" 
 */
function vcf_filter_form() {
  // query database
  $t_vocabs= vcf_query_tax_vocabs();
  $t_terms = vcf_query_tax_terms();
  // prepare variables
  $config = variable_get('views_contextual_filter_config');
  $conf_args = $config['args'];
  $conf_titles = $config['titles'];
  $arg_count = $config['argcount'];
  $searched_flag = 'ff';
  $url_arr = arg();
  
  // set form options
  $i = 0;
  $options = array();
  foreach ($conf_args as $conf_arg) {
    if (strpos($conf_arg, '=') === FALSE) {
      // set taxonomy options  
      foreach ($t_terms as $term) {
        if ($conf_arg == $term->vid) {
          $options[$i][$term->tid] = $term->name;
        }
      }
    }
    else {
      // set custom options
      $opt_arr = vcf_split_arg_string($conf_arg);
      foreach ($opt_arr as $opt) {
        $options[$i][$opt[0]] = $opt[1];
      }
    }
    $i++;
  }
  // if user searched before, set defaults
  $default_vals = array();
  if (in_array($searched_flag, $url_arr)) {
    $url_length = count($url_arr) - ($arg_count + 1);
    $arg_arr = array_slice($url_arr, $url_length, $arg_count);
    $i = 0;
    foreach ($arg_arr as $def_val) {
      if (strpos($def_val, ',') !== FALSE) {
        $default_vals[$i] = explode(',', $def_val);
      }
      else {
        $default_vals[$i] = array($def_val);
      }
      $i++;
    } 
  }
  else {
    for ($i=0; $i<$arg_count; $i++) {
      $default_vals[$i] = array();
    }
  }
  
  // create filter form
  $form['vcf_filter'] = array(
    '#type' => 'fieldset',
    '#tree' => TRUE,
  );
  
  for ($i=0; $i<$arg_count; $i++) {
    $form['vcf_filter'][$i] = array(
      '#type' => 'checkboxes',
      '#default_value' => $default_vals[$i],
      '#title' => check_plain($conf_titles[$i]),
      '#options' => $options[$i],
    );
  }
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Search'),
  );
  
  return $form;
}


/**
 *  "Filter form" submit handler
 */
function vcf_filter_form_submit($form, &$form_state) {
  
  // set variables
  $config = variable_get('views_contextual_filter_config');
  $url = $config['url'];
  $ignore = $config['ignore'];
  $arg_count = $config['argcount'];
  $searched_flag = 'ff';
  $url_arr = arg();
  
  // create arguments from form
  $args = '';
  foreach ($form_state['values']['vcf_filter'] as $arg) {
    $temp = array_keys($arg, '0');
    foreach ($temp as $key) {
      unset($arg[$key]);
    }
    if (empty($arg)) {
      $args .= '/' . $ignore;
    }
    else {
      $arg = implode(',', $arg );
      $args .='/' . $arg;
    }
  }
  
  // set trimmed path
  if ($url == 'stay_on_page') {
    if (in_array($searched_flag, $url_arr)) {
      $url_length = count($url_arr) - ($arg_count + 1);
    }
    else {
      $url_length = count($url_arr) - $arg_count;
    }
    $url = implode('/', array_slice($url_arr, 0, $url_length));
  }
  
  if ($url=='') {
    $url = variable_get('site_frontpage', 'node');
  }
  
  drupal_goto($url . $args . '/' . $searched_flag );
}

/**
 * Util function
 */
function vcf_split_arg_string($arg_string) {
  $arg_array = explode(",", $arg_string);
  $opts_array = array();
  foreach ($arg_array as $arg) {
    $opts_array[] = explode("=", $arg);
  }
  return $opts_array;
}

/**
 * Database query
 */
function vcf_query_tax_terms() {
  $query = db_select('taxonomy_term_data', 'ttd');
  $query->addField('ttd', 'vid');
  $query->addField('ttd', 'name');
  $query->addField('ttd', 'tid');
  return $query->execute()->fetchAll();
}

/**
 * Database query
 */
function vcf_query_tax_vocabs() {
  $query = db_select('taxonomy_vocabulary', 'tv');
  $query->addField('tv', 'vid');
  $query->addField('tv', 'name');
  return $query->execute()->fetchAll();
}
